package com.example.dzspring.dbtask.dao;


import com.example.dzspring.dbtask.model.Book;
import org.springframework.stereotype.Component;

import java.sql.*;
import java.time.LocalDate;

/**
 * Класс объекта доступа данных к таблице книги
 */
@Component
public class BookDaoBean {

    //поле соединения
    private final Connection connection;

    /**
     * Конструктор класса принимающий на вход объект текущего соединения
     * @param connection
     */
    public BookDaoBean(Connection connection) {
        this.connection = connection;
    }

    /**
     * Метод принимающий на вход поля объекта класса из модели книга и вставляющий их в
     * соответствующую таблицу базы
     * @param bookTitle имя книги
     * @param bookAuthor автор
     * @param dateAdded дата добавления книги в библиотеку
     * @throws SQLException выбрасываемое исключение
     */
    public void insertBook(String bookTitle, String bookAuthor, LocalDate dateAdded) throws SQLException {

        PreparedStatement insertQuery = connection.prepareStatement(
                "insert into books(title, author, date_added)\n" +
                        "values (?, ?, ?)"
        );

        insertQuery.setString(1, bookTitle);
        insertQuery.setString(2, bookAuthor);
        insertQuery.setDate(3, Date.valueOf(dateAdded));
        insertQuery.executeQuery();
        System.out.println("Книга добавлена!");

    }

    /**
     * Метод принимающий на вход название книги и возвращающий объект класса из модели
     * книга с соответствующим названием
     * @param title название книги
     * @return объект класса книга
     * @throws SQLException выбрасываемое исключение
     */
    public Book findBookByTitle(String title) throws SQLException {
        PreparedStatement selectQuery = connection.prepareStatement("select * from books where title = ?");
        selectQuery.setString(1, title);
        ResultSet resultSet = selectQuery.executeQuery();
        Book book = new Book();
        while (resultSet.next()) {
            book.setBookAuthor(resultSet.getString("author"));
            book.setBookTitle(resultSet.getString("title"));
            book.setDateAdded(resultSet.getDate("date_added"));

        }
        return book;
    }



}
